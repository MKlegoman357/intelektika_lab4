import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import MinMaxScaler


def run_kmeans(clusters, data, axis_labels, normalize_into_square=True):
    kmeans_data = data
    scaler = None

    if normalize_into_square:
        scaler = MinMaxScaler()
        scaler.fit(kmeans_data)
        kmeans_data = scaler.transform(kmeans_data)

    kmeans = KMeans(n_clusters=clusters)
    kmeans.fit(kmeans_data)
    labels = kmeans.predict(kmeans_data)

    plt.figure(figsize=(10, 4))

    plt.subplot(121)
    plt.title("input data")
    plt.xlabel(axis_labels[0])
    plt.ylabel(axis_labels[1])
    plt.scatter(data[:, 0], data[:, 1])

    plt.subplot(122)
    plt.title("clustered data")
    plt.xlabel(axis_labels[0])
    plt.ylabel(axis_labels[1])
    plt.scatter(data[:, 0], data[:, 1], c=labels)

    cluster_centers = kmeans.cluster_centers_

    if normalize_into_square:
        cluster_centers = scaler.inverse_transform(cluster_centers)

    plt.scatter(cluster_centers[:, 0], cluster_centers[:, 1], marker="x", c="r")

    score = silhouette_score(kmeans_data, labels)

    cluster_sizes = np.unique(labels, return_counts=True)
    total = len(data)
    cluster_sizes = [(size, float(size) / total) for size in cluster_sizes[1]]

    print("clusters:", clusters, "iterations:", kmeans.n_iter_, "cluster sizes:", cluster_sizes, "score:", score)


def process_data(data, clusters, columns):
    column_data = data[columns]
    run_kmeans(clusters, column_data.values, columns)


data = pd.read_csv("measure1_smartphone_sens.csv")
data = data.rename(columns=lambda column: column.strip())

# pd.plotting.scatter_matrix(data)

process_data(data, 2, ["AccelerationY", "X-AxisAngle(Pitch)"])
process_data(data, 2, ["AccelerationZ", "Y-AxisAngle(Roll)"])
process_data(data, 2, ["MagneticFieldZ", "Z-AxisAgle(Azimuth)"])

process_data(data, 3, ["AccelerationY", "Z-AxisAgle(Azimuth)"])
process_data(data, 3, ["AccelerationX", "MagneticFieldZ"])
process_data(data, 3, ["AccelerationX", "AccelerationZ"])

process_data(data, 4, ["AccelerationZ", "Z-AxisAgle(Azimuth)"])
process_data(data, 4, ["MagneticFieldX", "Z-AxisAgle(Azimuth)"])
process_data(data, 4, ["MagneticFieldZ", "Z-AxisAgle(Azimuth)"])

plt.show()
